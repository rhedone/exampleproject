## Description
An example project to be used for backend coding tests.

## Assumptions and caveats
- I have not taken any special care of the passwords as it was not part of the task.
- I'm not using branching or any extensive git commands.
- Limiting Unit tests to core areas, increasing test coverage would largely inflate the time spent on this coding test.
- Not implementing a code test coverage tool like Sonar.
- Not implementing an IOC framework for the limited scope of the test this would be overkill, manual IOC works just as good for small projects.
- Opting for readability over speed as no performance criteria were specified.
- Using name as a unique identifier (i.e. a Slug) instead of a proper unique server generated ID.
- Using mostly IntelliJ default templates generation (for eg. toString()), ideally a better template would be made.
- There is little concern for security in place. For instance the username is both used as a unique identifier for "database" entries, shown in the leaderboard and to authenticate the player.
- The API endpoint documentation is written by hand instead of using a generation tool. 

### Requirements and used technologies
1. Gradle 7.4.2
2. Docker version 20.10.14
3. Docker Desktop 4.8.1
4. Git version 2.36.1.windows.1
5. IntelliJ 2021.3.3 (Community Edition)

## Run locally
In order to run the project you can simply run the main class in your IDE or use docker.

### Docker
1. Checkout this project through Git, the gitlab project page has a guide on how to do this
2. Navigate to the project root (where the `docker-compose` file is located).
3. run the following command to build the image`docker compose build`.
4. run the following command to run the image`docker compose up`, you can add the `-d` command to start it in the background.
## Endpoints

#### Register user
- Request method: POST
- Endpoint: /yolan/user
- Request header: none
- Request body:
  - `{"name":""","password": ""}`
- Response body: none
- Response contentType: none
- Response codes:
  - successful: 201(Created) User registered
  - failed: 400 (Bad Request) Duplicate username
- Description: Register a new user.
- test curl command:
  - `curl -i -X POST -d '{"name":"aUserName","password": "SuperSafePasswordCorrectHorseBatteryStaple"}' localhost:7000/yolan/user`

#### Login user
- Request method: POST
- Endpoint: /yolan/user/login
- Request header: none
- Request body:
  - `{"name":""","password": ""}`
- Response contentType:application/json
- Response body:
  - the token is a valid Java UUID'{"token":" ***Token***"}'
- Response codes:
  - successful: 201 (Created) Token created
  - failed: 400 (Bad Request) Login failed
- Description: Login an existing user and create an attached token to be used in future API calls.
- test curl command:
  - `curl -i -X POST -d '{"name":"aUserName","password": "SuperSafePasswordCorrectHorseBatteryStaple"}' localhost:7000/yolan/user/login`

#### Submit Palindrome
- Request method: POST
- Endpoint: /yolan/palindrome/verify
- Request header:
  - token: ***replaceWithToken***
- Request body:
  - `{"palindrome":"thisIsNotAPalindrome"}`
- Response contentType:application/json
- Response body: none
- Response codes:
  - successful: 200 (OK) Palindrome accepted
  - failed: 400 (Bad Request) Submitted word was not a palindrome
- Description: Receive a word test to see if it is a palindrome. If the word is a palindrome the player will receive a score.
- test curl command:
  - `curl -i -X POST -H "token: ***replaceWithToken***" -d '{"palindrome":"radar"}' localhost:7000/yolan/palindrome/verify`

#### Request player's leaderboard
- Request method: GET
- Endpoint: /yolan/palindrome/leaderboard
- Request header:
  - token: ***replaceWithToken***
- Response contentType:application/json
- Response body:
  - `{"value":"","palindrome":"","player":""}`
- Response codes:
  - successful: 200 (OK)
  - failed: 400 (Bad Request)
- Description: The leader board of the player attached to the provided token
- test curl command:
  - `curl -i -X GET -H "token: 2a7fbb2a-4372-48bb-bf75-2b04a233f1e0" localhost:7000/yolan/palindrome/leaderboard`

#### Request global leaderboard
- Request method: GET
- Endpoint: /yolan/palindrome/leaderboard
- Request header: none
- Response contentType:application/json
- Response body:
  - `{"value":"","palindrome":"","player":""}`
- Response codes:
  - successful:200 (OK)
  - failed: 400 (Bad Request)
- Description: Request the global leaderboard
- test curl command:
  - `curl -i -X GET -H localhost:7000/yolan/palindrome/leaderboard`