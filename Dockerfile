# Stage 3: embedded SQL LITE
# Based on https://github.com/KEINOS/Dockerfile_of_SQLite3/
FROM alpine:3.16.0 AS sqllite-alpine

RUN \
  apk update && \
  apk upgrade && \
  apk add \
    alpine-sdk \
    build-base  \
    tcl-dev \
    tk-dev \
    mesa-dev \
    jpeg-dev \
    libjpeg-turbo-dev

RUN  \
    wget -O sqlite.tar.gz https://www.sqlite.org/src/tarball/sqlite.tar.gz?r=release && \
     tar xvfz sqlite.tar.gz

RUN \
  ./sqlite/configure --prefix=/usr && \
  make && \
  make install

#COPY /usr/bin/sqlite3 /usr/bin/sqlite3

CMD /usr/bin/sqlite3

# Stage 1: Gradle build
FROM gradle:7.4.2-jdk17 AS gradle-build

COPY . /home/gradle/src
WORKDIR /home/gradle/src

RUN gradle build --no-daemon

# Stage 2: Create Java app container
FROM openjdk:12-jdk-alpine

EXPOSE 7000

RUN mkdir /app

COPY --from=gradle-build /home/gradle/src/build/libs/Yolan-1.0-SNAPSHOT.jar /app

WORKDIR /app

CMD java -jar Yolan-1.0-SNAPSHOT.jar