package org.yolan.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import org.yolan.external.UserConnector;
import org.yolan.user.model.User;

import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;

//tokens could be stored as a key value instead of list
public class TokenController {

    private final String issuer;
    private final String secret;
    private final String userKey;

    private final UserConnector userConnector;

    public TokenController(String issuer,
                           String secret,
                           String userKey,
                           UserConnector userConnector) {
        this.issuer = issuer;
        this.secret = secret;
        this.userKey = userKey;
        this.userConnector = userConnector;
    }

    public String generateToken(User user) {
        Algorithm algorithm = Algorithm.HMAC256(secret);
        return JWT.create()
                .withIssuer(issuer)
                .withClaim(userKey, user.getName())
                .withIssuedAt(Date.from(Instant.now()))
                .withExpiresAt(Date.from(Instant.now().plus(1, ChronoUnit.HOURS)))
                .sign(algorithm);
    }

    public User getUser(String token) throws IOException, SQLException {
        String userName = readToken(token, secret, issuer, userKey);
        Optional<User> user = userConnector.read(userName);
        return user.orElseThrow();
    }


    private static String readToken(final String token,
                                    final String secret,
                                    final String issuer,
                                    final String userKey) {
        Algorithm algorithm = Algorithm.HMAC256(secret);
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer(issuer)
                .build(); //Reusable verifier instance
        DecodedJWT jwt = verifier.verify(token);

        return jwt.getClaim(userKey).asString();
    }
}
