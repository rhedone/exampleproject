package org.yolan.token.model;

import java.util.Objects;
import java.util.UUID;

public class Token {
    private final UUID uuid;

    public Token(UUID uuid) {
        this.uuid = uuid;
    }

    public Token(String uuid) {
        this(UUID.fromString(uuid));
    }

    public String getTokenValue() {
        return uuid.toString();
    }

    @Override
    public String toString() {
        return "Token{" +
                "uuid=" + uuid +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return Objects.equals(uuid, token.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
