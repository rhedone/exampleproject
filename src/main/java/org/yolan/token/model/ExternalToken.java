package org.yolan.token.model;

public class ExternalToken {
    private final String token;

    public ExternalToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "ExternalToken{" +
                "token='" + token + '\'' +
                '}';
    }
}
