package org.yolan.util;

import io.javalin.Javalin;
import org.yolan.leaderboard.GlobalLeaderboardHandler;
import org.yolan.leaderboard.PlayerLeaderboardHandler;
import org.yolan.palindrome.PalindromeHandler;
import org.yolan.user.LogInUserHandler;
import org.yolan.user.RegisterUserHandler;

public class Router {
    public static void route(final Javalin app,
                             final RegisterUserHandler registerUserHandler,
                             final LogInUserHandler logInUserHandler,
                             final PalindromeHandler palindromeHandler,
                             final PlayerLeaderboardHandler playerLeaderboardHandler,
                             final GlobalLeaderboardHandler globalLeaderboardHandler) {
        app.post(Path.REGISTER, registerUserHandler);
        app.post(Path.LOGIN, logInUserHandler);
        app.post(Path.PALINDROME_VERIFY, palindromeHandler);
        app.get(Path.PLAYER_LEADERBOARD, playerLeaderboardHandler);
        app.get(Path.GLOBAL_LEADERBOARD, globalLeaderboardHandler);
        setUpDefaults(app);
    }

    private static void setUpDefaults(Javalin app) {
        app.before(ctx -> {
            Logger.debug("Received request to url: '" + ctx.fullUrl() + "' with body " + ctx.body());
        });
        app.after(ctx -> {
            Logger.debug("Returning " + ctx.status() + " with body " + ctx.body());
        });
    }
}

