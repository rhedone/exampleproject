package org.yolan.util;

import java.time.Instant;

//TODO just a simple self implemented logger, replace with an actual logging framework.
public class Logger {
    private enum LogLevel {
        INFO, DEBUG, WARN, ERROR
    }

    public static void info(String logMessage) {
        log(LogLevel.INFO, logMessage);
    }

    public static void debug(String logMessage) {
        log(LogLevel.DEBUG, logMessage);
    }

    public static void warn(String logMessage) {
        log(LogLevel.WARN, logMessage);
    }

    public static void error(String logMessage) {
        log(LogLevel.ERROR, logMessage);
    }

    private static void log(LogLevel logLevel, String logMessage) {
        System.out.println(Instant.now().toString() + " | " + logLevel + ": " + logMessage);
    }
}
