package org.yolan.util;

import java.util.List;

//Using replace is not a performant way of removing the characters, but it is more readable, and we have no performance targets.
public class PalindromeCleaner {
    private static final List<String> toReplace = List.of(" ", ".", ";", ",", "!", "?");

    public static String clean(final String s) {
        String subString = s;
        for (String character : toReplace) {
            subString = subString.replace(character, "");
        }
        return subString.toLowerCase();
    }
}
