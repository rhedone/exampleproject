package org.yolan.util;

import com.google.gson.Gson;

import java.lang.reflect.Type;

// Would prefer this not to be a singleton but it helps with testing and I don't want to over-engineer it.
public class JsonHelper {

    private static JsonHelper jsonHelper = null;

    private JsonHelper() {
    }

    public static JsonHelper getInstance() {
        if (jsonHelper == null) {
            jsonHelper = new JsonHelper();
        }
        return jsonHelper;
    }

    public <T> String toJson(T t) {
        Gson gson = new Gson();
        return gson.toJson(t);
    }

    public <T> T fromJson(String s, Type t) {
        Gson gson = new Gson();
        return gson.fromJson(s, t);

    }
    public <T> T fromJson(String s, Class<T> t) {
        Gson gson = new Gson();
        return gson.fromJson(s, t);
    }
}
