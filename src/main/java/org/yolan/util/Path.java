package org.yolan.util;

public class Path {
    // Internal substrings
    private static final String ROOT = "/yolan";
    private static final String USER = "/user";
    private static final String PALINDROME = "/palindrome";
    public static final String LEADERBOARD = "/leaderboard";

    // to be used complete paths
    public static final String REGISTER = ROOT + USER;
    public static final String LOGIN = ROOT + USER + "/login";

    public static final String PALINDROME_VERIFY = ROOT + PALINDROME + "/verify";
    public static final String PLAYER_LEADERBOARD = ROOT + PALINDROME + LEADERBOARD;
    public static final String GLOBAL_LEADERBOARD = ROOT + PALINDROME + LEADERBOARD + "/global";
}
