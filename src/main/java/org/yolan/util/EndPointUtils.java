package org.yolan.util;

import io.javalin.http.Context;
import io.javalin.http.HttpCode;
import org.jetbrains.annotations.NotNull;
import org.yolan.model.ActionResult;
import org.yolan.edge.model.Result;

public class EndPointUtils {
    public static void resolveResponseCode(@NotNull Context ctx, ActionResult actionResult) {
        ctx.status(actionResult.getHttpCode());
    }

    public static void resolveResponseBody(@NotNull Context ctx, Result result) {
        if (result.getReturnValue() != null && !result.getReturnValue().isBlank()) {
            ctx.json(result.getReturnValue());
        }
    }
    public static void resolveBadRequest(@NotNull Context ctx){
        Logger.warn("Received a bad request");
        ctx.status(HttpCode.BAD_REQUEST);
    }
}
