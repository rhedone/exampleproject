package org.yolan.leaderboard;

import org.yolan.leaderboard.model.ScoreStorable;
import org.yolan.external.GlobalLeaderboard;
import org.yolan.external.ScoreConnector;
import org.yolan.user.model.User;
import org.yolan.util.JsonHelper;
import org.yolan.util.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LeaderBoardController {
    private final int leaderboardSize;
    private final ScoreConnector scoreConnector;
    private final GlobalLeaderboard globalLeaderboard;
    private final JsonHelper jsonHelper;

    public LeaderBoardController(int leaderboardAmount, ScoreConnector scoreConnector, GlobalLeaderboard globalLeaderboard, JsonHelper jsonHelper) {
        this.leaderboardSize = leaderboardAmount;
        this.scoreConnector = scoreConnector;
        this.globalLeaderboard = globalLeaderboard;
        this.jsonHelper = jsonHelper;
    }

    public List<ScoreStorable> getPlayerLeaderboard(User user) throws SQLException {
        List<ScoreStorable> scoreStorables = scoreConnector.read(user.getName());
        return orderScoresDescending(scoreStorables);
    }

    public List<ScoreStorable> getGlobalLeaderboard() throws SQLException {
        List<ScoreStorable> scoreStorables = globalLeaderboard.getLeaderboard();
        return orderScoresDescending(scoreStorables);
    }

    public void checkGlobalLeaderboard(ScoreStorable score) throws SQLException {
        List<ScoreStorable> scores = globalLeaderboard.getLeaderboard();
        List<ScoreStorable> updatedScores = new ArrayList<>(scores);
        updatedScores.add(score);
        updatedScores = orderScoresDescending(updatedScores);

        updatedScores = updatedScores.subList(0, getGlobalLeaderboardSize(scores.size()));

        if (!scores.equals(updatedScores)) {
            Logger.info("Updating global leaderboard with score " + score);
            // TODO: 2022-06-06 fix it
//            globalLeaderboardDao.store(updatedScores);
        }
    }

    private int getGlobalLeaderboardSize(int size) {
        return Math.min(size + 1, (leaderboardSize));
    }

    private List<ScoreStorable> orderScoresDescending(List<ScoreStorable> updatedScores) {
        return updatedScores.stream()
                .sorted((o1, o2) -> Integer.compare(o2.getValue(), o1.getValue()))
                .collect(Collectors.toList());
    }
}
