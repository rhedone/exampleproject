package org.yolan.leaderboard.model;

public class ScoreStorable {
    private final int value;
    private final String palindrome;
    private final String user;

    public ScoreStorable(int value, String palindrome, String user) {
        this.value = value;
        this.palindrome = palindrome;
        this.user = user;
    }

    public int getValue() {
        return value;
    }

    public String getPalindrome() {
        return palindrome;
    }

    public String getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "ScoreStorable{" +
                "value=" + value +
                ", palindrome='" + palindrome + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
