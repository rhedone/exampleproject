package org.yolan.leaderboard.model;

public class ExternalScore {
    private final String user;
    private final String palindrome;
    private final int value;

    public ExternalScore(String user, String palindrome, int value) {
        this.value = value;
        this.palindrome = palindrome;
        this.user = user;
    }

    public int getValue() {
        return value;
    }

    public String getPalindrome() {
        return palindrome;
    }

    public String getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "LeaderboardEntry{" +
                "score=" + value +
                ", palindrome='" + palindrome + '\'' +
                ", user=" + user +
                '}';
    }
}
