package org.yolan.leaderboard;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;
import org.yolan.leaderboard.model.ScoreStorable;
import org.yolan.model.ActionResult;
import org.yolan.edge.model.Result;
import org.yolan.leaderboard.model.ExternalScore;
import org.yolan.token.TokenController;
import org.yolan.user.model.User;
import org.yolan.util.EndPointUtils;
import org.yolan.util.JsonHelper;
import org.yolan.util.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class PlayerLeaderboardHandler implements Handler {

    private final TokenController tokenController;
    private final LeaderBoardController leaderBoardController;
    private final JsonHelper jsonHelper;

    public PlayerLeaderboardHandler(JsonHelper jsonHelper, TokenController tokenController,
                                    LeaderBoardController leaderBoardController) {
        this.tokenController = tokenController;
        this.leaderBoardController = leaderBoardController;
        this.jsonHelper = jsonHelper;
    }

    @Override
    public void handle(@NotNull Context ctx) throws Exception {
        String token = ctx.header("token");
        final Result result;

        result = handleRequest(token);

        EndPointUtils.resolveResponseCode(ctx, result.getActionResult());
        EndPointUtils.resolveResponseBody(ctx, result);
    }


    public Result handleRequest(String token) throws Exception {
        final List<ScoreStorable> playerLeaderboard;

        User user = tokenController.getUser(token);
        Logger.info("Returning leaderboard for " + user);
        playerLeaderboard = leaderBoardController.getPlayerLeaderboard(user);

        String body = formatScore(playerLeaderboard);

        return new Result(ActionResult.SUCCESSFUL, body);
    }

    private String formatScore(List<ScoreStorable> scoreStorable) {
        List<ExternalScore> externalScores = scoreStorable
                .stream()
                .map(ss -> new ExternalScore(ss.getUser(), ss.getPalindrome(), ss.getValue()))
                .collect(Collectors.toList());

        return jsonHelper.toJson(externalScores);
    }
}
