package org.yolan.dependency;

import org.yolan.external.GlobalLeaderboard;
import org.yolan.external.ScoreConnector;
import org.yolan.external.UserConnector;
import org.yolan.leaderboard.GlobalLeaderboardHandler;
import org.yolan.leaderboard.LeaderBoardController;
import org.yolan.leaderboard.PlayerLeaderboardHandler;
import org.yolan.palindrome.PalindromeController;
import org.yolan.palindrome.PalindromeHandler;
import org.yolan.token.TokenController;
import org.yolan.user.LogInUserHandler;
import org.yolan.user.RegisterUserHandler;
import org.yolan.user.UserController;
import org.yolan.util.JsonHelper;

import java.sql.SQLException;

public class DiController {
    private final int leaderboardSize;
    private final String issuer;
    private final String secret;
    private final String userKey;

    private RegisterUserHandler registerUserHandler;
    private LogInUserHandler logInUserHandler;
    private PalindromeHandler palindromeHandler;
    private PlayerLeaderboardHandler playerLeaderboardHandler;
    private GlobalLeaderboardHandler globalLeaderboardHandler;

    public DiController(int leaderboardSize, String issuer, String secret, String userKey) {
        this.leaderboardSize = leaderboardSize;
        this.issuer = issuer;
        this.secret = secret;
        this.userKey = userKey;
    }

    public void init() throws SQLException {

        final JsonHelper jsonHelper = JsonHelper.getInstance();

        final UserConnector userConnector = new UserConnector();
        final ScoreConnector scoreConnector = new ScoreConnector();
        final GlobalLeaderboard globalLeaderboard = new GlobalLeaderboard(scoreConnector);

        // Controllers
        final UserController userController = new UserController(userConnector);
        final TokenController tokenController = new TokenController(issuer, secret, userKey, userConnector);
        final LeaderBoardController leaderBoardController = new LeaderBoardController(leaderboardSize, scoreConnector, globalLeaderboard, jsonHelper);
        final PalindromeController palindromeController = new PalindromeController(scoreConnector, leaderBoardController);

        // API request handlers
        registerUserHandler = new RegisterUserHandler(userController, jsonHelper);
        logInUserHandler = new LogInUserHandler(jsonHelper, userController, tokenController);
        palindromeHandler = new PalindromeHandler(jsonHelper, palindromeController, tokenController);
        playerLeaderboardHandler = new PlayerLeaderboardHandler(jsonHelper, tokenController, leaderBoardController);
        globalLeaderboardHandler = new GlobalLeaderboardHandler(jsonHelper, tokenController, leaderBoardController);
    }

    public RegisterUserHandler getRegisterUserHandler() {
        return registerUserHandler;
    }

    public LogInUserHandler getLogInUserHandler() {
        return logInUserHandler;
    }

    public PalindromeHandler getPalindromeHandler() {
        return palindromeHandler;
    }

    public PlayerLeaderboardHandler getPlayerLeaderboardHandler() {
        return playerLeaderboardHandler;
    }

    public GlobalLeaderboardHandler getGlobalLeaderboardHandler() {
        return globalLeaderboardHandler;
    }
}
