package org.yolan.external;

import org.sqlite.SQLiteConfig;
import org.sqlite.javax.SQLiteConnectionPoolDataSource;

import javax.sql.PooledConnection;
import java.sql.Connection;
import java.sql.SQLException;

public abstract class SqlConnector {

    private static PooledConnection pooledConnection;

    static {
        try {

            SQLiteConfig config = new org.sqlite.SQLiteConfig();

            SQLiteConnectionPoolDataSource SQLiteConnectionPoolDataSource = new SQLiteConnectionPoolDataSource(config);

            pooledConnection = SQLiteConnectionPoolDataSource.getPooledConnection();

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

    public static void close() throws SQLException {
        pooledConnection.close();
    }

    public SqlConnector() throws SQLException {
        createTable();
    }

    protected Connection getConnector() throws SQLException {
        return pooledConnection.getConnection();
    }

    public abstract void createTable() throws SQLException;
}
