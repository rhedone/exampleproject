package org.yolan.external;

import org.yolan.leaderboard.model.ScoreStorable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ScoreConnector extends SqlConnector {
    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS score" +
            "(id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
            " user TEXT NOT NULL," +
            " value INTEGER NOT NULL," +
            " palindrome TEXT NOT NULL)";


    private static final String SELECT_SCORES_FOR_USER = "SELECT id, user, value, palindrome FROM score WHERE user = ? ORDER BY VALUE DESC";
    private static final String SELECT_SCORES_FOR_USER_TOP = "SELECT id, user, value, palindrome FROM score ORDER BY value DESC LIMIT ? ";

    private static final String INSERT_USER = "INSERT INTO score (user, value, palindrome) VALUES (?, ?, ?)";

    public ScoreConnector() throws SQLException {
        super();
    }

    @Override
    public void createTable() throws SQLException {
        try (Connection connector = getConnector();
             PreparedStatement statement = connector.prepareStatement(CREATE_TABLE)) {
            statement.executeUpdate();
        }
    }

    public List<ScoreStorable> read(String userName) throws SQLException {
        List<ScoreStorable> scores = new ArrayList<>();
        try (Connection connector = getConnector();
             PreparedStatement statement = connector.prepareStatement(SELECT_SCORES_FOR_USER)) {
            statement.setString(1, userName);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String user = resultSet.getString("user");
                int value = resultSet.getInt("value");
                String palindrome = resultSet.getString("palindrome");
                scores.add(new ScoreStorable(value, palindrome, user));
            }
            return scores;
        }
    }

    public List<ScoreStorable> readTop(int i) throws SQLException {
        List<ScoreStorable> scores = new ArrayList<>();
        try (Connection connector = getConnector();
             PreparedStatement statement = connector.prepareStatement(SELECT_SCORES_FOR_USER_TOP)) {
            statement.setInt(1, i);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String user = resultSet.getString("user");
                int value = resultSet.getInt("value");
                String palindrome = resultSet.getString("palindrome");
                scores.add(new ScoreStorable(value, palindrome, user));
            }
            return scores;
        }
    }

    public void create(ScoreStorable scoreStorable) throws SQLException {
        try (Connection connector = getConnector();
             PreparedStatement statement = connector.prepareStatement(INSERT_USER)) {
            statement.setString(1, scoreStorable.getUser());
            statement.setInt(2, scoreStorable.getValue());
            statement.setString(3, scoreStorable.getPalindrome());
            statement.execute();
        }
    }
}
