package org.yolan.external;

import org.yolan.user.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class UserConnector extends SqlConnector {
    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS user" +
            "(id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
            " name TEXT UNIQUE  NOT NULL," +
            " password TEXT NOT NULL);";

    private static final String SELECT_USER = "SELECT id, name, password FROM user WHERE name = ?; ";
    private static final String INSERT_USER = "INSERT INTO user (name, password) VALUES (?, ?); ";

    public UserConnector() throws SQLException {
        super();
    }

    @Override
    public void createTable() throws SQLException {

        try (Connection connector = getConnector();
             PreparedStatement statement = connector.prepareStatement(CREATE_TABLE)) {
            statement.executeUpdate();
        }
    }

    public Optional<User> read(String name) throws SQLException {
        Optional<User> user = Optional.empty();
        try (Connection connector = getConnector();
             PreparedStatement statement = connector.prepareStatement(SELECT_USER)) {
            statement.setString(1, name);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String nameExternal = resultSet.getString("name");
                String password = resultSet.getString("password");
                user = Optional.of(new User(nameExternal, password));
            }
            return user;
        }
    }

    public void create(User user) throws SQLException {
        try (Connection connector = getConnector();
             PreparedStatement statement = connector.prepareStatement(INSERT_USER)) {
            statement.setString(1, user.getName());
            statement.setString(2, user.getPassword());
            statement.execute();
        }
    }
}
