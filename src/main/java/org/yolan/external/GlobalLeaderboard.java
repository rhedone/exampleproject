package org.yolan.external;

import org.yolan.leaderboard.model.ScoreStorable;

import java.sql.SQLException;
import java.util.List;

// TODO: 2022-06-06 this should be a separate cache.
public class GlobalLeaderboard {
    private final ScoreConnector scoreConnector;

    public GlobalLeaderboard(ScoreConnector scoreConnector) {
        this.scoreConnector = scoreConnector;
    }

    public List<ScoreStorable> getLeaderboard() throws SQLException {
        return scoreConnector.readTop(10);
    }
}
