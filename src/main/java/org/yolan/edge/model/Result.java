package org.yolan.edge.model;

import org.yolan.model.ActionResult;

public class Result {
    private final ActionResult actionResult;
    private final String returnValue;

    public Result(ActionResult actionResult, String returnValue) {
        this.actionResult = actionResult;
        this.returnValue = returnValue;
    }

    public Result(ActionResult actionResult) {
        this(actionResult, "");
    }

    public ActionResult getActionResult() {
        return actionResult;
    }

    public String getReturnValue() {
        return returnValue;
    }

    @Override
    public String toString() {
        return "Result{" +
                "actionResult=" + actionResult +
                ", returnValue='" + returnValue + '\'' +
                '}';
    }
}
