package org.yolan;

import io.javalin.Javalin;
import org.yolan.dependency.DiController;
import org.yolan.external.SqlConnector;
import org.yolan.util.Logger;
import org.yolan.util.Router;

import java.sql.SQLException;

public class Main {

    //TODO make these configurable
    public static final int PORT = 8080;
    public static final int LEADERBOARD_SIZE = 3;
    public static final String ISSUER = "org.yolan.testapp";
    public static final String SECRET = "secret";
    public static final String USER_KEY = "user";

    public static void main(String[] args) throws SQLException {
        Javalin app = Javalin.create().start(PORT);

        //Manual IOC linking,
        DiController diController = new DiController(LEADERBOARD_SIZE, ISSUER, SECRET, USER_KEY);
        diController.init();

        Router.route(app,
                diController.getRegisterUserHandler(),
                diController.getLogInUserHandler(),
                diController.getPalindromeHandler(),
                diController.getPlayerLeaderboardHandler(),
                diController.getGlobalLeaderboardHandler());

        shutdownHook();
    }

    private static void shutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                Logger.info("Starting Shutdown hook");
                SqlConnector.close();
            } catch (Exception e) {
                Logger.error(e.toString());
            }
        }));
    }
}