package org.yolan.model;

import io.javalin.http.HttpCode;

public enum ActionResult {
    CREATED(HttpCode.CREATED),
    SUCCESSFUL(HttpCode.OK),
    ALREADY_EXISTS(HttpCode.BAD_REQUEST),
    PASSWORD_MISMATCH(HttpCode.BAD_REQUEST),
    NOT_FOUND(HttpCode.BAD_REQUEST),
    INVALID(HttpCode.BAD_REQUEST);

    private final HttpCode httpCode;

    ActionResult(HttpCode httpCode) {
        this.httpCode = httpCode;
    }

    public HttpCode getHttpCode() {
        return httpCode;
    }
}
