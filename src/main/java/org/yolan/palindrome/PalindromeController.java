package org.yolan.palindrome;

import org.yolan.leaderboard.LeaderBoardController;
import org.yolan.leaderboard.model.ScoreStorable;
import org.yolan.model.ActionResult;
import org.yolan.external.ScoreConnector;
import org.yolan.user.model.User;
import org.yolan.util.Logger;
import org.yolan.util.PalindromeCleaner;

import java.sql.SQLException;

public class PalindromeController {

    private final ScoreConnector scoreConnector;
    private final LeaderBoardController leaderBoardController;

    public PalindromeController(ScoreConnector scoreConnector, LeaderBoardController leaderBoardController) {
        this.scoreConnector = scoreConnector;
        this.leaderBoardController = leaderBoardController;
    }

    public ActionResult handle(String string) {

        final ActionResult actionResult;
        boolean palindrome = isPalindrome(string);
        if (palindrome) {
            actionResult = ActionResult.SUCCESSFUL;
        } else {
            actionResult = ActionResult.INVALID;
        }
        Logger.info("The string '" + string + "' is a palindrome: " + actionResult);
        return actionResult;
    }

    public void scoreEntry(String palindrome, User user) throws SQLException {
        int score = calculateScore(palindrome);

        ScoreStorable scoreStorable = new ScoreStorable(score, palindrome, user.getName());
        Logger.info("Storing: '" + scoreStorable);
        scoreConnector.create(scoreStorable);
        leaderBoardController.checkGlobalLeaderboard(scoreStorable);
    }

    private boolean isPalindrome(final String palindrome) {
        final String stringCleaned = PalindromeCleaner.clean(palindrome);
        return stringCleaned.equals(reverseString(stringCleaned));
    }

    private String reverseString(final String s) {
        return new StringBuilder(s).reverse().toString();
    }


    // if this scoring ever gets more complicated it should be it's own controller.
    private int calculateScore(String palindrome) {
        return palindrome.length();
    }
}
