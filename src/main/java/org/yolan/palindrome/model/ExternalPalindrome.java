package org.yolan.palindrome.model;

public class ExternalPalindrome {
    private final String palindrome;

    public ExternalPalindrome(String palindrome) {
        this.palindrome = palindrome;
    }

    public String getPalindrome() {
        return palindrome;
    }

    @Override
    public String toString() {
        return "ExternalPalindrome{" +
                "palindrome='" + palindrome + '\'' +
                '}';
    }
}
