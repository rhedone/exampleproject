package org.yolan.palindrome;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;
import org.yolan.model.ActionResult;
import org.yolan.edge.model.Result;
import org.yolan.palindrome.model.ExternalPalindrome;
import org.yolan.token.TokenController;
import org.yolan.user.model.User;
import org.yolan.util.EndPointUtils;
import org.yolan.util.JsonHelper;
import org.yolan.util.Logger;

public class PalindromeHandler implements Handler {
    private final JsonHelper jsonHelper;
    private final PalindromeController palindromeController;
    private final TokenController tokenController;

    public PalindromeHandler(JsonHelper jsonHelper, PalindromeController palindromeController, TokenController tokenController) {
        this.jsonHelper = jsonHelper;
        this.palindromeController = palindromeController;
        this.tokenController = tokenController;
    }

    @Override
    public void handle(@NotNull Context ctx) throws Exception {
        String body = ctx.body();
        ExternalPalindrome externalPalindrome = jsonHelper.fromJson(body, ExternalPalindrome.class);

        Logger.info("Received request: " + externalPalindrome);
        String token = ctx.header("token");
        User user = tokenController.getUser(token);
        if (user != null) {
            Result result = handleRequest(externalPalindrome, user);

            EndPointUtils.resolveResponseCode(ctx, result.getActionResult());
            EndPointUtils.resolveResponseBody(ctx, result);
        } else {
            EndPointUtils.resolveBadRequest(ctx);
        }
    }

    public Result handleRequest(ExternalPalindrome externalPalindrome, User user) throws Exception {
        String palindrome = externalPalindrome.getPalindrome();
        ActionResult actionResult = palindromeController.handle(palindrome);
        if (actionResult.equals(ActionResult.SUCCESSFUL)) {
            palindromeController.scoreEntry(palindrome, user);
        }
        return new Result(actionResult);
    }
}
