package org.yolan.user;

import org.yolan.edge.model.Result;
import org.yolan.user.model.User;
import org.yolan.util.JsonHelper;
import org.yolan.util.Logger;

public class RegisterUserHandler extends UserHandler {

    private final UserController userController;

    public RegisterUserHandler(UserController userController, JsonHelper jsonHelper) {
        super(jsonHelper);
        this.userController = userController;
    }

    @Override
    public Result handleUserRequest(User user) throws Exception {
        Logger.info("Registering new user " + user);
        return new Result(userController.registerUser(user));
    }
}
