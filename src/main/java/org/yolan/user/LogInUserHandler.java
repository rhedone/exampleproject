package org.yolan.user;

import org.yolan.edge.model.Result;
import org.yolan.model.ActionResult;
import org.yolan.token.TokenController;
import org.yolan.token.model.ExternalToken;
import org.yolan.user.model.User;
import org.yolan.util.JsonHelper;
import org.yolan.util.Logger;

public class LogInUserHandler extends UserHandler {

    private final UserController userController;
    private final TokenController tokenController;

    public LogInUserHandler(JsonHelper jsonHelper, UserController userController, TokenController tokenController) {
        super(jsonHelper);
        this.userController = userController;
        this.tokenController = tokenController;
    }

    @Override
    public Result handleUserRequest(User user) throws Exception {
        Logger.info("Logging in new user " + user);
        ActionResult actionResult = userController.logIn(user);
        String formattedToken = "";
        if (actionResult == ActionResult.SUCCESSFUL) {
            String token = tokenController.generateToken(user);
            formattedToken = formatToken(token);
        }
        return new Result(actionResult, formattedToken);
    }

    private String formatToken(String token) {
        String returnValue;

        ExternalToken externalToken = new ExternalToken(token);
        returnValue = jsonHelper.toJson(externalToken);

        return returnValue;
    }
}
