package org.yolan.user.model;

public class ExternalUser {
    private final String name;
    private final String password;

    public ExternalUser(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "ExternalUser{" +
                "Name='" + name + '\'' +
                ", Password='" + password + '\'' +
                '}';
    }
}
