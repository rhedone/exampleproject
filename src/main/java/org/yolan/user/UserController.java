package org.yolan.user;

import org.yolan.external.UserConnector;
import org.yolan.model.ActionResult;
import org.yolan.user.model.User;
import org.yolan.util.Logger;

import java.sql.SQLException;
import java.util.Optional;

public class UserController {

    private final UserConnector userConnector;

    public UserController(UserConnector userConnector) {
        this.userConnector = userConnector;
    }

    public ActionResult registerUser(User user) throws SQLException {
        final ActionResult result;

        Optional<User> storedUser = userConnector.read(user.getName());

        if (storedUser.isPresent()) {
            Logger.info("Received duplicate user: " + user);
            result = ActionResult.ALREADY_EXISTS;
        } else {
            userConnector.create(user);
            result = ActionResult.CREATED;
        }
        return result;
    }

    public ActionResult logIn(User requestUser) throws SQLException {
        ActionResult result;
        Optional<User> o = userConnector.read(requestUser.getName());
        if (o.isEmpty()) {
            Logger.info("Unknown user: " + requestUser);
            result = ActionResult.NOT_FOUND;
        } else if (o.get().getPassword().equals(requestUser.getPassword())) {
            Logger.info("Successful user login " + requestUser);
            result = ActionResult.SUCCESSFUL;
        } else {
            Logger.info("Wrong password for : " + requestUser);
            // Possible hiding other cases as password mismatch
            result = ActionResult.PASSWORD_MISMATCH;
        }
        return result;
    }
}
