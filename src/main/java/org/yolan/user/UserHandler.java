package org.yolan.user;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;
import org.yolan.edge.model.Result;
import org.yolan.user.model.ExternalUser;
import org.yolan.user.model.User;
import org.yolan.util.EndPointUtils;
import org.yolan.util.JsonHelper;
import org.yolan.util.Logger;

public abstract class UserHandler implements Handler {
    protected final JsonHelper jsonHelper;

    protected UserHandler(JsonHelper jsonHelper) {
        this.jsonHelper = jsonHelper;
    }

    @Override
    public void handle(@NotNull Context ctx) throws Exception {
        String body = ctx.body();
        ExternalUser externalUser = jsonHelper.fromJson(body, ExternalUser.class);
        User user = convertUser(externalUser);

        Logger.info("Received request: " + user);
        Result result = handleUserRequest(user);

        EndPointUtils.resolveResponseCode(ctx, result.getActionResult());
        EndPointUtils.resolveResponseBody(ctx, result);
    }


    public abstract Result handleUserRequest(User user) throws Exception;

    private User convertUser(ExternalUser externalUser) {
        return new User(externalUser.getName(),
                externalUser.getPassword());
    }
}
