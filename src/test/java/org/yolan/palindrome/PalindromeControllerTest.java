package org.yolan.palindrome;

import org.junit.jupiter.api.Test;
import org.yolan.leaderboard.LeaderBoardController;
import org.yolan.model.ActionResult;
import org.yolan.external.ScoreConnector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class PalindromeControllerTest {

    @Test
    void isPalindromeSuccessSimple() {
        // Actual variables

        // Mocked variables
        ScoreConnector scoreConnector = mock(ScoreConnector.class);
        LeaderBoardController leaderBoardController = mock(LeaderBoardController.class);

        // Class to test
        PalindromeController palindromeController = new PalindromeController(scoreConnector, leaderBoardController);
        ActionResult result = palindromeController.handle("radar");

        //Verifications
        assertEquals(result, ActionResult.SUCCESSFUL);
    }

    @Test
    void isPalindromeSuccessSentence() {
        // Actual variables

        // Mocked variables
        ScoreConnector scoreConnector = mock(ScoreConnector.class);
        LeaderBoardController leaderBoardController = mock(LeaderBoardController.class);

        // Class to test
        PalindromeController palindromeController = new PalindromeController(scoreConnector, leaderBoardController);
        ActionResult result = palindromeController.handle(",A man? a plan! a canal; Panama.");

        //Verifications
        assertEquals(result, ActionResult.SUCCESSFUL);
    }

    @Test
    void isPalindromeInvalidSimple() {
        // Actual variables

        // Mocked variables
        ScoreConnector scoreConnector = mock(ScoreConnector.class);
        LeaderBoardController leaderBoardController = mock(LeaderBoardController.class);

        // Class to test
        PalindromeController palindromeController = new PalindromeController(scoreConnector, leaderBoardController);
        ActionResult result = palindromeController.handle("invalid");

        //Verifications
        assertEquals(result, ActionResult.INVALID);
    }

    @Test
    void isPalindromeInvalidSentence() {
        // Actual variables

        // Mocked variables
        ScoreConnector scoreConnector = mock(ScoreConnector.class);
        LeaderBoardController leaderBoardController = mock(LeaderBoardController.class);

        // Class to test
        PalindromeController palindromeController = new PalindromeController(scoreConnector, leaderBoardController);
        ActionResult result = palindromeController.handle("This sentence is not a palindrome.");

        //Verifications
        assertEquals(result, ActionResult.INVALID);
    }
}