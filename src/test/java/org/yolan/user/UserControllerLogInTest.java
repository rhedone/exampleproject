package org.yolan.user;

import org.junit.jupiter.api.Test;
import org.yolan.external.UserConnector;
import org.yolan.model.ActionResult;
import org.yolan.user.model.User;
import org.yolan.util.JsonHelper;

import java.sql.SQLException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;


class UserControllerLogInTest {

    @Test
    void registerUserSuccess() throws SQLException {
        // Actual variables
        final String newName = "name";
        final String newPassword = "password";
        final User newUser = new User(newName, newPassword);

        final String StoredName = "name";
        final String StoredPassword = "password";
        final Optional<User> storedUser = Optional.of(new User(StoredName, StoredPassword));

        // Mocked variables
        UserConnector userConnector = mock(UserConnector.class);
        JsonHelper jsonHelper = mock(JsonHelper.class);

        // Mock returns
        doReturn(storedUser).when(userConnector).read(newName);

        // Class to test
        UserController userController = new UserController(userConnector);
        ActionResult actionResult = userController.logIn(newUser);

        //Verifications
        assertEquals(ActionResult.SUCCESSFUL, actionResult);
    }

    @Test
    void registerUserFailNull() throws SQLException {
        // Actual variables
        final String newName = "name";
        final String newPassword = "password";
        final User newUser = new User(newName, newPassword);


        // Mocked variables
        UserConnector userConnector = mock(UserConnector.class);
        JsonHelper jsonHelper = mock(JsonHelper.class);

        // Mock returns
        doReturn(Optional.empty()).when(userConnector).read(newName);

        // Class to test
        UserController userController = new UserController(userConnector);
        ActionResult actionResult = userController.logIn(newUser);

        //Verifications
        assertEquals(ActionResult.NOT_FOUND, actionResult);
    }


    @Test
    void registerUserFailWrongPassword() throws SQLException {
        // Actual variables
        final String newName = "name";
        final String newPassword = "password";
        final User newUser = new User(newName, newPassword);

        final String StoredName = "name";
        final String StoredPassword = "theWrongPassword";
        final Optional<User> storedUser = Optional.of(new User(StoredName, StoredPassword));

        // Mocked variables
        UserConnector UserConnector = mock(UserConnector.class);
        JsonHelper jsonHelper = mock(JsonHelper.class);

        // Mock returns
        doReturn(storedUser).when(UserConnector).read(newName);

        // Class to test
        UserController userController = new UserController(UserConnector);
        ActionResult actionResult = userController.logIn(newUser);

        //Verifications
        assertEquals(ActionResult.PASSWORD_MISMATCH, actionResult);
    }

}