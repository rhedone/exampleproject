package org.yolan.user;

import org.junit.jupiter.api.Test;
import org.yolan.external.UserConnector;
import org.yolan.model.ActionResult;
import org.yolan.user.model.User;

import java.sql.SQLException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

class UserControllerRegisterTest {

    @Test
    void registerUserSuccessful() throws SQLException {
        // Actual variables
        final String nameNew = "name";
        final String password = "password";
        final User user = new User(nameNew, password);

        // Mocked variables
        UserConnector userConnector = mock(UserConnector.class);

        // Mock returns
        doReturn(Optional.empty()).when(userConnector).read(nameNew);
        // Class to test
        UserController userController = new UserController(userConnector);
        ActionResult actionResult = userController.registerUser(user);

        //Verifications
        assertEquals(ActionResult.CREATED, actionResult);
    }

    @Test
    void registerUserFailAlreadyExists() throws SQLException {
        // Actual variables
        final String nameNew = "name";
        final String password = "password";
        final User user = new User(nameNew, password);
        Optional<User> storedUser = Optional.of(user);

        // Mocked variables
        UserConnector userConnector = mock(UserConnector.class);

        // Mock returns
        doReturn(storedUser).when(userConnector).read(nameNew);

        // Class to test
        UserController userController = new UserController(userConnector);
        ActionResult actionResult = userController.registerUser(user);

        //Verifications
        assertEquals(ActionResult.ALREADY_EXISTS, actionResult);
    }

    @Test
    void registerUserFailAlreadyExistsDifferentPassword() throws SQLException {
        // Actual variables
        final String nameNew = "name";
        final String password = "password";
        final User user = new User(nameNew, password);
        final User storedUser = new User(nameNew, "aDifferentPassword");

        Optional<User> storedUserOptional = Optional.of(storedUser);

        // Mocked variables
        UserConnector userConnector = mock(UserConnector.class);

        // Mock returns
        doReturn(storedUserOptional).when(userConnector).read(nameNew);

        // Class to test
        UserController userController = new UserController(userConnector);
        ActionResult actionResult = userController.registerUser(user);

        //Verifications
        assertEquals(ActionResult.ALREADY_EXISTS, actionResult);
    }
}